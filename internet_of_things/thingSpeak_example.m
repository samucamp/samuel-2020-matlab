clc; clear all;

%%%Dados retirados do canal thingSpeak Channel ID e Chave de escrita.
channelID = ID;
writeKey = 'key';

%%%Cria-se um objeto para se utilizar e configurar a porta serial.
%Configura��o da Porta
ComTest = serial('COM9');
%Cria um objeto de nome ComTest para configura e utilizar a comunica��o serial

%%%Podem-se configurar os par�metros da porta da seguinte maneira:
%set(ComTest,'BaudRate',9600);
%set(ComTest,'DataBits',8);
%set(ComTest,'Parity','none');
%set(ComTest,'StopBits',1);
%set(ComTest,'FlowControl','none');
% Tamanho do Buffer de entrada
%set(ComTest,'InputBufferSize',512);
% Tempo para receber algum dado (ms)
%set(ComTest,'Timeout',5000);
% O programa aguarda este tempo para receber um dado, caso o dado n�o chegue, o programa segue adiante

%%%Obs: Caso os par�metros acima mencionados n�o sejam digitados, o MatLat configura-os automaticamente.

%%%Para abrir a porta serial:
%Abre a porta Serial
fopen(ComTest);
%A porta deve ser aberta antes de ser utilizada.

%%%Para envio de dados:
%Envia uma string %s, Caractere %c, etc.
%%fprintf(ComTest,'%u',dado);

while 1
    tStamp = datetime('now');%-minutes(9):minutes(1):datetime('now');
    
    %data = randi(10,10,3);%
    
    %%%Para receber dados:
    % Os dados recebidos s�o armazenados no vetor dado
    data = fread(ComTest,1);
    %o segundo valor '2' � o n�mero de bytes a receber
    %caso um byte n�o seja recebido o programa aguarda
    %at� estourar o tempo limite, que pode ser configurado
    %por set(ComTest,'Timeout',tempo); tempo em ms

    thingSpeakWrite(channelID,data,'WriteKey',writeKey,'TimeStamp',tStamp);
    pause(15);
end

    %%%Para finalizar a porta pode utilizar os comandos abaixo:
    %%fclose(ComTest);
    %%delete(ComTest)
    %%delete(instrfindall) %finaliza todas as portas dispon�veis
    %%clear ComTest