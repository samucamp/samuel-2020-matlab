classdef app1 < matlab.apps.AppBase

    % Properties that correspond to app components
    properties (Access = public)
        UIFigure         matlab.ui.Figure
        Menu             matlab.ui.container.Menu
        Menu2            matlab.ui.container.Menu
        EditFieldLabel   matlab.ui.control.Label
        val1             matlab.ui.control.NumericEditField
        EditField2Label  matlab.ui.control.Label
        val2             matlab.ui.control.NumericEditField
        EditField3Label  matlab.ui.control.Label
        result           matlab.ui.control.NumericEditField
        button           matlab.ui.control.Button
        SliderLabel      matlab.ui.control.Label
        slider           matlab.ui.control.Slider
        graph            matlab.ui.control.UIAxes
        EditField4Label  matlab.ui.control.Label
        slider_value     matlab.ui.control.NumericEditField
        ListBoxLabel     matlab.ui.control.Label
        ListBox          matlab.ui.control.ListBox
        EditField5Label  matlab.ui.control.Label
        EditField5       matlab.ui.control.EditField
        DropDownLabel    matlab.ui.control.Label
        DropDown         matlab.ui.control.DropDown
        UIAxes           matlab.ui.control.UIAxes
    end

    methods (Access = private)

        % Button pushed function: button
        function buttonButtonPushed(app, event)
            a = app.val1.Value;
            b = app.val2.Value;
            app.result.Value = a + b;
        end

        % Value changed function: slider
        function sliderValueChanged(app, event)

        end

        % Value changing function: slider
        function sliderValueChanging(app, event)
            changingValue = event.Value;
            
            x = 0:0.1:50;
            y = sin(changingValue*x);
            plot(app.graph,x,y);
            
            app.slider_value.Value = changingValue;
        end

        % Value changed function: ListBox
        function ListBoxValueChanged(app, event)
            value = app.ListBox.Value;
            app.EditField5.Value = value;
        end

        % Value changed function: DropDown
        function DropDownValueChanged(app, event)
            value = app.DropDown.Value;
            x = 0.0:0.01:50;
            y = sin(x)
            
            if value == 'Option 1'
                scatter(app.UIAxes,x,y);
            end
            if value == 'Option 2'
                    plot(app.UIAxes,x,y)
            end 
           
        end
    end

    % App initialization and construction
    methods (Access = private)

        % Create UIFigure and components
        function createComponents(app)

            % Create UIFigure
            app.UIFigure = uifigure;
            app.UIFigure.Position = [100 100 640 480];
            app.UIFigure.Name = 'UI Figure';

            % Create Menu
            app.Menu = uimenu(app.UIFigure);
            app.Menu.Text = 'Menu';

            % Create Menu2
            app.Menu2 = uimenu(app.UIFigure);
            app.Menu2.Text = 'Menu2';

            % Create EditFieldLabel
            app.EditFieldLabel = uilabel(app.UIFigure);
            app.EditFieldLabel.BackgroundColor = [1 1 0];
            app.EditFieldLabel.HorizontalAlignment = 'right';
            app.EditFieldLabel.Position = [11 449 56 22];
            app.EditFieldLabel.Text = 'Edit Field';

            % Create val1
            app.val1 = uieditfield(app.UIFigure, 'numeric');
            app.val1.ValueDisplayFormat = '%.3f';
            app.val1.HorizontalAlignment = 'center';
            app.val1.BackgroundColor = [1 1 0];
            app.val1.Position = [82 449 100 22];

            % Create EditField2Label
            app.EditField2Label = uilabel(app.UIFigure);
            app.EditField2Label.BackgroundColor = [1 1 0];
            app.EditField2Label.HorizontalAlignment = 'right';
            app.EditField2Label.Position = [11 419 62 22];
            app.EditField2Label.Text = 'Edit Field2';

            % Create val2
            app.val2 = uieditfield(app.UIFigure, 'numeric');
            app.val2.ValueDisplayFormat = '%.3f';
            app.val2.HorizontalAlignment = 'center';
            app.val2.BackgroundColor = [1 1 0];
            app.val2.Position = [88 419 100 22];

            % Create EditField3Label
            app.EditField3Label = uilabel(app.UIFigure);
            app.EditField3Label.BackgroundColor = [0 1 0];
            app.EditField3Label.HorizontalAlignment = 'right';
            app.EditField3Label.Position = [11 389 62 22];
            app.EditField3Label.Text = 'Edit Field3';

            % Create result
            app.result = uieditfield(app.UIFigure, 'numeric');
            app.result.ValueDisplayFormat = '%.3f';
            app.result.Editable = 'off';
            app.result.HorizontalAlignment = 'center';
            app.result.BackgroundColor = [0 1 0];
            app.result.Position = [88 389 100 22];

            % Create button
            app.button = uibutton(app.UIFigure, 'push');
            app.button.ButtonPushedFcn = createCallbackFcn(app, @buttonButtonPushed, true);
            app.button.BackgroundColor = [0.651 0.651 0.651];
            app.button.Position = [41 349 100 22];

            % Create SliderLabel
            app.SliderLabel = uilabel(app.UIFigure);
            app.SliderLabel.HorizontalAlignment = 'right';
            app.SliderLabel.Position = [11 269 36 22];
            app.SliderLabel.Text = 'Slider';

            % Create slider
            app.slider = uislider(app.UIFigure);
            app.slider.ValueChangedFcn = createCallbackFcn(app, @sliderValueChanged, true);
            app.slider.ValueChangingFcn = createCallbackFcn(app, @sliderValueChanging, true);
            app.slider.Position = [68 278 150 3];

            % Create graph
            app.graph = uiaxes(app.UIFigure);
            title(app.graph, 'Title')
            xlabel(app.graph, 'X')
            ylabel(app.graph, 'Y')
            app.graph.Position = [1 66 300 185];

            % Create EditField4Label
            app.EditField4Label = uilabel(app.UIFigure);
            app.EditField4Label.HorizontalAlignment = 'right';
            app.EditField4Label.Position = [11 39 62 22];
            app.EditField4Label.Text = 'Edit Field4';

            % Create slider_value
            app.slider_value = uieditfield(app.UIFigure, 'numeric');
            app.slider_value.Editable = 'off';
            app.slider_value.Position = [88 39 100 22];

            % Create ListBoxLabel
            app.ListBoxLabel = uilabel(app.UIFigure);
            app.ListBoxLabel.HorizontalAlignment = 'right';
            app.ListBoxLabel.Position = [211 437 48 22];
            app.ListBoxLabel.Text = 'List Box';

            % Create ListBox
            app.ListBox = uilistbox(app.UIFigure);
            app.ListBox.ValueChangedFcn = createCallbackFcn(app, @ListBoxValueChanged, true);
            app.ListBox.Position = [274 387 100 74];

            % Create EditField5Label
            app.EditField5Label = uilabel(app.UIFigure);
            app.EditField5Label.HorizontalAlignment = 'right';
            app.EditField5Label.Position = [201 359 62 22];
            app.EditField5Label.Text = 'Edit Field5';

            % Create EditField5
            app.EditField5 = uieditfield(app.UIFigure, 'text');
            app.EditField5.Position = [278 359 100 22];

            % Create DropDownLabel
            app.DropDownLabel = uilabel(app.UIFigure);
            app.DropDownLabel.HorizontalAlignment = 'right';
            app.DropDownLabel.Position = [341 269 66 22];
            app.DropDownLabel.Text = 'Drop Down';

            % Create DropDown
            app.DropDown = uidropdown(app.UIFigure);
            app.DropDown.ValueChangedFcn = createCallbackFcn(app, @DropDownValueChanged, true);
            app.DropDown.Position = [422 269 100 22];
            app.DropDown.Value = 'Option 3';

            % Create UIAxes
            app.UIAxes = uiaxes(app.UIFigure);
            title(app.UIAxes, 'Title')
            xlabel(app.UIAxes, 'X')
            ylabel(app.UIAxes, 'Y')
            app.UIAxes.Position = [311 66 300 185];
        end
    end

    methods (Access = public)

        % Construct app
        function app = app1

            % Create and configure components
            createComponents(app)

            % Register the app with App Designer
            registerApp(app, app.UIFigure)

            if nargout == 0
                clear app
            end
        end

        % Code that executes before app deletion
        function delete(app)

            % Delete UIFigure when app is deleted
            delete(app.UIFigure)
        end
    end
end