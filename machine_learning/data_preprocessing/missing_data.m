%% DEALING WITH MISSING DATA PART 1
clc;clear;
% importing dataset 
data = readtable('data_preprocessing\Data_1.csv')

% data preprocessing
% handling missing values

% method 1: deleting rows or columns

% complete_data = rmmissing(data); %row with NaN will be removed
% complete_data = rmmissing(data,2); % columns with NaN will be removed
% 
% data = complete_data;

% method 2: using mean

M_Age = mean(data.Age, 'omitnan') % option to omit NaN and do mean calc
U_Age = fillmissing(data.Age, 'constant',M_Age) % fill missing data with mean

%updating data in the main table
data.Age = U_Age


%% DEALING WITH MISSING DATA PART 2
clc;clear;
% importing dataset 2
data = readtable('data_preprocessing\Data_2.csv')

% method 1: deleting rows or columns
%%%%%%%%%%%%%%rmmissing(dataset, 2=column, 'props', minimum)
restricted_missing = rmmissing(data, 2, 'MinNumMissing', 2)

%% DEALING WITH MISSING DATA PART 3
clc;clear;
% importing dataset 2
data = readtable('data_preprocessing\Data_3.csv')

% method 3: nom numeric data
data.Opinion = categorical(data.Opinion)
freq_opinion = mode(data.Opinion) 

Opinion = fillmissing(data.Opinion,'constant',cellstr(freq_opinion))
data.Opinion = Opinion
