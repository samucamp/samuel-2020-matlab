%%%% FEATURE SCALING
%% STANDARDIZATION
clc;clear;
% importing dataset 
data = readtable('data_preprocessing\Data_4.csv')
average = mean(data.Age)
stand_age = (data.Age - average)/std(data.Age)
data.Age = stand_age;
data.Age

%% NORMALIZATION 
clc;clear;
% importing dataset 
data = readtable('data_preprocessing\Data_4.csv')
norm_age = (data.Age - min(data.Age))/(max(data.Age) - min(data.Age))
data.Age = norm_age;
data.Age
