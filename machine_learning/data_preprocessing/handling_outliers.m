%%%%% HANDLING OUTLIERS
%% Part 1
clc;clear;
data = readtable('data_preprocessing\Data_5.csv')

outliers = isoutlier(data.Age)
~outliers
data = data(~outliers,:)

%% Part 2
clc;clear;
data = readtable('data_preprocessing\Data_5.csv')

%%%%%%%%%filloutliers(data, fill, method)
Age = filloutliers(data.Age, 'clip', 'mean')
data.Age = Age