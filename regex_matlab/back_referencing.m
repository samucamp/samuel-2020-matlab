
regexp('axa','(\w)x\1','match')

regexp('axb','(\w)x\1','match') 

regexp('axcbc','(\w)x(\w)\1\2','match') 

regexp('axcac','(\w)x(\w)\1\2','match') 

regexp('b','(a)?b\1','match') 

regexp('ab','(a)?b\1','match') 

regexp('aba','(a)?b\1','match') 

regexp('ab','(?>a)?b\1','match')

regexp('ab','(a)+b\1','match')


regexp('cat=cat','([atc]+)=\1','match')

regexp('cat=cat','([atc])+=\1','match')

regexp('axa','(?<tag1>\w)?x\k<tag1>','match') 