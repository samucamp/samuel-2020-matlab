a = 9
whos

a = [9 8 8; 10 8 9; 8 7 9]
whos

a = 'hello world'
whos 

a(2,:) = a(1, :)
a(3,1) = 'hi how are you'
a(3,1) = 'hi how are '

a = string('Hi how are you')
a = [string('Hi how are you') string('Hello world'); string('Nouman') string('Azam') ]
a(1,2)

a = [8 9 9; 4 5 6]
a(2,2) 

a(1,3) = 9+1 

a = logical([1 0 1 0;1 1 0 1])
b = 89
C = 45 

D = b + C


% this is the declaration of the variables 
b = 89;

%this is the declaration for variable c
C = 45; 

% This is the final result
D = b + C

% operações com : 
1:5 
1:2:10 
-100:2:-90 
10:-2:0 

% gerando random
x = rand(10,1) 
x = rand(10,10)

x(1:3,:)
x(1:2:end,:) 

 