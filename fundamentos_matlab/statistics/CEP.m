clc
clear

%gera valores aleat�rios (8+t)x101
open_system('ALEATORIO.slx')
sim('ALEATORIO.slx')
close_system('ALEATORIO.slx')

n = 8; %n�mero de elementos por amostra
aux = 1; %para repetir a verifica��o de pontos inst�veis

%tabela cartas de controle
A3 = 1.099; 
B3 = 0.185;
B4 = 1.815;

while aux == 1
    X_barra = mean(aleatorio,2);
    S_barra = std(aleatorio,0,2);

    XLC = mean(X_barra);
    SLC = mean(S_barra);
    XLSC = XLC + (A3*SLC);
    XLIC = XLC - (A3*SLC);
    SLSC = SLC*B4;
    SLIC = SLC*B3;
    for i = 1:101
        if (((X_barra(i)) > XLSC)|((X_barra(i)) < XLIC)|((S_barra(i)) > SLSC)|((S_barra(i)) < SLIC))
            aleatorio(i,:) = []; %apaga toda linha do elemento inst�vel
            aux = 1;
        else
            aux = 0;
        end
    end
end

X_barra = mean(aleatorio,2);
S_barra = std(aleatorio,0,2);

XLC = mean(X_barra);
SLC = mean(S_barra);
XLSC = XLC + (A3*SLC);
XLIC = XLC - (A3*SLC);
SLSC = SLC*B4;
SLIC = SLC*B3;
    
AXLSC = XLC + ((2/3)*A3*SLC);
AXLIC = XLC - ((2/3)*A3*SLC);
ASLSC = SLC + ((2/3)*((SLC*B4) - SLC));
ASLIC = SLC - ((2/3)*(SLC - (SLC*B3)));

open_system('CEPGLP.slx')
sim('CEPGLP.slx')