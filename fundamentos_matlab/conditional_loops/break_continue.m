%%% BREAK STATEMENT

fprintf('Please enter a number and i am going to tell you the next prime number after that \n'); 
N = input(''); 

for i = N:inf
	if isprime(i)
		fprintf('The number prime number we evaluated is %d \n',i);
		break;
	end 
end 

%%% CONTINUE STATEMENT

fprintf('Please enter the students data for exam no.1 \n'): 
d = 1; 

for i=1:10
	fprintf('Wheter student No. %d has appeared in teh exam [Y|N] \n',i);
	
	c1 = input('','s');
	if c1 == 'N'
		continue; 
	end  

	fprintf('Please enter the grade for the student \n');
	grade(d) = input(''); 
	d = d + 1; 
end 
