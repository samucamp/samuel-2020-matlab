x = rand(10,1);

y = rand(10,1);

z = rand(10,1);

%plot(x,y); 

%figure, plot(x,y);  
% for holding a plot so that it is not over written 

subplot(2,2,1), plot(x,y); 
subplot(2,2,2), plot(x,z); 
subplot(2,2,3), plot(y,z); 
subplot(2,2,4), plot(x,x); 


 %% additional options
 %% Figure 1 / View / Property Editor
 %% possible to customize graph properties
 %% File / Generate Code 
 %% A code with all changes will be generated

 %% Other possibility is to generate plots with PLOTS on top
 %% create data variables, example A B Code
 %% select data variables and choose plot3