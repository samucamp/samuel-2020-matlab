Age = [38;15;49;78;54]
Height = [4;5;9;8;10]
Weigth = [85;90;78;56;42] 
T = table(Age,Height,Weigth)

LastNames = {'Nouman','Asif','Shahid','Joe', 'Azam'}
T = table(Age,Height,Weigth, 'RowNames',LastNames)
T.Properties.RowNames = LastNames

T = table(Age,Height,Weigth, 'VariableNames',{'Ages_years','Height_foots','Weight_Kgs'})
T.Properties.RowNames = LastNames

% adding descriptions, units and accessing individual columns
%---- Note: The table T has been defined based on the commands in the previous tutorial ---- 
 
t1 = table([45;78;89],[54;78;89],[1;0;2])

t1.Properties.VariableNames = {'Variable1', 'Variable2', 'Variable3'}

T.Properties.VariableUnits = {'Yrs','Foots','Kgs'}
summary(T)
T.Properties.VariableDescriptions('Ages_years') = {'This variable contains ages in year'}

T.Properties.VariableDescriptions = {'This variable contains ages in years', 'This represents height', 'This is the weight variable'}

T.Ages_years
T.Weight_Kgs
