clear; close; clc;

f  = @(x) x^2; % funcao f(x)

% forward differences
ff = @(x,h) (f(x+h) - f(x)/h);

% centered differences
fc = @(x, dx) (f(x+dx) - f(x-dx))/(2*dx);

% calculo da derivada
x = 2;
h = 0.1;
fprinf("\nPor diferença progressiva = %.4f", ff(x,h))
fprinf("\nPor diferença central = %.4f", fc(x,h))

