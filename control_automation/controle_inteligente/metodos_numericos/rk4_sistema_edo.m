clear; close all; clc;

%%dados para integra��o
h = 1;
t = 0:h:10;

% sistema de EDO
T = 10000;
M = 100;
mdot = 5;
g = 9.8;

f1 = @(t, x1, x2) x2;
f2 = @(t, x1, x2) T/(M-mdot*t)-g;

% Condi��es Iniciais
x1 = 0; 
x2 = 0;

% Solu��o do sistema de EDO para duas equa��es

for i=1:length(t)
    fprintf('\nt = %d, x1 = %f, x2 = %f\n', t(i), x1, x2);
    
    k11 = f1(t(i), x1, x2);
    k21 = f2(t(i), x1, x2);
    
    k12 = f1(t(i)+0.5*h,x1+0.5*h*k11,x2+0.5*h*k21);
    k22 = f2(t(i)+0.5*h,x1+0.5*h*k11,x2+0.5*h*k21);
    
    k13 = f1(t(i)+0.5*h,x1+0.5*h*k12,x2+0.5*h*k22);
    k23 = f2(t(i)+0.5*h,x1+0.5*h*k12,x2+0.5*h*k22);
    
    k14 = f1(t(i)+h,x1+h*k13,x2+h*k23);
    k24 = f1(t(i)+h,x1+h*k13,x2+h*k23);
    
    x1 = x1 + (1/6)*h*(k11+2*k12+2*k13+k14);
    x2 = x2 + (1/6)*h*(k21+2*k22+2*k23+k24);
end
