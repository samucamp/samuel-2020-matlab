clc; clear all; close all

ndados = 1000;
T = 0.1;

%% treinamento
% entrada
u = [3*ones(1,ndados) ones(1,ndados) 2*ones(1,ndados) 0.5*ones(1,ndados) ...
    0.25*ones(1,ndados) 1.25*ones(1,ndados) 0.75*ones(1,ndados)];

[i,j] = size(u);

% modelo discretizado
h = zeros(1,j);
for k = 2:j
    h(k) = ((T*u(k))+(0.1*h(k-1))/(T+0.1));
end

% dados entrada neuronio
cont = 1;
for k = 2:j
    Xtrain(:,cont) = [h(k-1); u(k)];
    Ytrain(cont) = h(k);
    cont = cont + 1;
end

% RNA
net = feedforwardnet(2);
net.layers{1}.transferFcn = 'tansig';
net.layers{2}.transferFcn = 'purelin';
net = train(net,Xtrain,Ytrain);

%% teste
ut = 4.25*ones(1,ndados);

[i,j] = size(ut);

h = zeros(1,j);
for k = 2:j
    h(k) = ((T*ut(k))+(0.1*h(k-1))/(T+0.1));
end

cont = 1;
for k = 2:j
    Xtest(:,cont) = [h(k-1); ut(k)];
    cont = cont + 1;
end

yh = net(Xtest);

plot(yh)
hold
plot(h)
hold
