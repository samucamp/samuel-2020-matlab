clc;
clear all;

figure
t = 0:0.2:25;
x = ones(1, length(t));
plot(t,x)

hold on
t = 25:0.2:100;
x = (1 + ((t-25)/5).^2).^-1;
plot(t,x)
grid on

figure
t = 0:0.2:50;
x=0;
%x = zeros(0, length(t));
plot(t,x)

hold on
t = 50:0.2:100;
x = (1 + ((t-50)/5).^-2).^-1;
plot(t,x)
grid on
