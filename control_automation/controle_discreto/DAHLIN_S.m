function [Dz] = DAHLIN_S(Ts)
%DAHLIN_S Summary of this function goes here
%   Detailed explanation goes here
char P;
% Obten��o da Gz %
G = tf([26.85],[1 1.96 14.92]);
tal = Ts/5;
T = (1/10)*(tal);
Gz = c2d(G,T) 
p = Gz.Numerator{1};raizes = roots(p);

% C�lculo do Fz %
syms 'z';
syms 'b';
a = exp(-T/tal);
b = 1 - a;
Fz= tf([b],[1 -a],T,'Variable','z')
J = Fz/(1-Fz);
Cz = (1/Gz)*J;
Lz = Cz;


% Reprojeto de Gz ---- Tem que entrar com polo ring %
syms 'B';
if raizes(1)<-0.5
    if raizes(1)>-1.0
    P = -raizes(1);
    k = roots(Cz.Denominator{1});
        if (k(1) - (-P))<0.00001
            var = Cz.Denominator{1};
            var2 = var(1);
            termo1 = 1+(-k(1));
            termo2 = -k(2) + (-k(2)*-k(1)) -k(3) +(-k(3)*-k(1));
            termo3 = -k(3)*-k(2) + (-k(3)*-k(2)*-k(1));
            termo = tf([termo1 termo2 termo3 0],[1],T,'Variable','z');
            Cz.Denominator{1} = termo.Denominator{1};
            (Cz)/termo;
            fprintf('O controlador com reprojeto ser�: ')
            Dz = ((Cz)/termo)*(1/var2)
        else Lz
        end
    end  
end
end

