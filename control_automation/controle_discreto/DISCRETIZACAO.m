s = tf('s')
Cs = 0.028189*((s+0.1)/(s+0.01))*((s^2+1.96*s+14.92)/(s^2+0.98*s+0.24)) %controlador avan�o-atraso
Gs = 26.85/ (s^2 + 1.96*s + 14.92) %planta
margin(Cs*Gs)
syms 'Wn'
Wn = 0.724 %mudar conforme gr�fico de bode  (margin CS GS)
Tmin = (5*pi/180)*(2/Wn)
Tmax = (15*pi/180)*(2/Wn)
open_system('CONTROLADOR.slx')
Ts = (Tmin+Tmax)/2
Gz = c2d(Cs, Ts, 'zoh') %ZOH
Gz = c2d(Cs, Ts, 'matched') %mapeamento de polos
Gz = c2d(Cs, Ts, 'tustin') %bilinear (sem prewarping)
%MF = 60 ..... e = 0.6
%pre-warping (s+p)=>(s+pb), pb = (2/Ts)*tan(p*Ts/2),
%caso 2a ordem => p = wn, acha pb.
%opt = c2dOptions('Method','tustin','PrewarpFrequency',.5)
%sysd = c2d(sysc,.1,opt);