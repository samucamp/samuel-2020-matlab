%RAGAZZINI_S Summary of this function goes here
%   Detailed explanation goes here

% Obten��o da Gz %
Gs = tf([26.85],[1 1.96 14.92])
E = 0.7 %definio (sobressinal de 5%)
ts = 8 %definido
wn = 4/(ts*E)
wd = wn*sqrt(1-(E^2))
Td = (2*pi/wd)
Ta = (1/20.5)*(Td)

Gz = c2d(Gs,Ta)
syms 'z'
DeltaZ1 = (exp(-E*wn))*( cos(wd) + (i*sin(wd)) ) 
DeltaZ2 = (exp(-E*wn))*( cos(wd) - (i*sin(wd)) ) 
DeltaZ = tf([1],[poly([DeltaZ1 DeltaZ2])],Ta)

% Obten��o Hz %
p = Gz.Numerator{1} %caso tenha polo ring
ring = roots(p)     %caso tenha polo ring
%bo = (DEFINIR MANUALMENTE)/(1-ring) 
bo = 0.3089/(1-ring)

Hz = tf([bo -bo*ring],[poly([DeltaZ1 DeltaZ2])],Ta)

% Obten��o Dz %
Dz = (1/Gz)*(Hz*(1/(1-Hz)))