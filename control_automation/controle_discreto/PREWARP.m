s = tf('s')
Cs = 0.028189*((s+0.1)/(s+0.01))*((s^2+1.96*s+14.92)/(s^2+0.98*s+0.24)) %controlador avan�o-atraso
Gs = 26.85/ (s^2 + 1.96*s + 14.92) %planta
margin(Cs*Gs)
syms 'Wn'
Wn = 0.724 %mudar conforme gr�fico de bode  (margin CS GS)
Tmin = (5*pi/180)*(2/Wn)
Tmax = (15*pi/180)*(2/Wn)
Ts = (Tmin+Tmax)/2
Cz = c2d(Cs, Ts, 'tustin')

%pb = (2/Ts)*tan(p*Ts/2)
G = 0.028189
P1 = 0.1
P1B = (2/Ts)*tan(P1*Ts/2)
P2 = 0.01
P2B = (2/Ts)*tan(P2*Ts/2)
W1 = sqrt(14.92)
E1 = 1.96/(W1*2)
W1B = (2/Ts)*tan(W1*Ts/2)
W2 = sqrt(0.24)
E2 = 0.98/(W2*2)
W2B = (2/Ts)*tan(W2*Ts/2)

CsB = G*((s+P1B)/(s+P2B))*( (s^2 +(2*E1*W1B*s)+ W1B^2) / (s^2 +(2*E2*W2B*s)+ W2B^2))

Dz = c2d(CsB, Ts, 'tustin') %bilinear