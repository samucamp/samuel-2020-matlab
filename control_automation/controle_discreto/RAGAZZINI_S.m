function [Dz] = RAGAZZINI_S(ts)
%RAGAZZINI_S Summary of this function goes here
%   Detailed explanation goes here

% Obten��o da Gz %
Gs = tf([26.85],[1 1.96 14.92])
tal = ts/5
Ta = (1/8)*(tal)
Gz = c2d(Gs,Ta)

% Obten��o Hz %
p = Gz.Numerator{1}
ring = roots(p)
a = exp((-1/tal)*Ta)
bo = (1-a)/(1-ring)
Hz = tf([bo -bo*ring],[1 -a 0],Ta)

% Obten��o Dz %
Dz = (1/Gz)*(Hz*(1/(1-Hz)))
end

